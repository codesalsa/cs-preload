<?php
/*
Plugin Name: cs-preload
Plugin URI: http://codesalsa.net/projects/codesalsa-pre-load/
Description: CS Preload is a lightweight wordpress plugin to integrate a customizable Preloader feature to any website with ease.
Version: 1.0
Author: Code Salsa
Author URI: http://codesalsa.net
License: MIT
*/


// Add the stylesheet to the header

function cs_preLoad_style(){
	wp_enqueue_style( 'preLoadStyles', plugins_url('/assets/css/preLoad.css', __FILE__), false);
}

add_action('wp_enqueue_scripts', 'cs_preLoad_style');

function cs_insert_preLoad() {
	
?>
	<div class="preloader">
		<div class="loadscreen">
			<div class="loadscreen-in">
				
			</div>
		</div>
	</div>
	<script>
	jQuery(document).ready( function() {
		
		jQuery(window).on('load', function() {
            jQuery('.loadscreen').fadeOut();
            jQuery('.preloader').delay(350).fadeOut('slow');
        });
	});
	</script>
<?php
}

add_action('wp_footer', 'cs_insert_preLoad');

function cs_preLoad_jquery() {
    wp_enqueue_script( 'jquery' );
}    

add_action('wp_enqueue_scripts', 'cs_preLoad_jquery');

?>